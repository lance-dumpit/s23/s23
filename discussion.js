// CRUD Operations

// Insert Documents (CREATE)

/*
	Syntax: 
		Inserting One Document
			-db.collectionName.insertOne({
					"fieldA": "valueA",
					"fieldB": "valueB",

			})

*/

db.users.insertOne({
	"firstName": "Jane",
	"lastName": "Doe",
	"age": 21,
	"email": "janedoe@gmail.com",
	"department": "none"

});

/*

Insert Many Documents

	Syntax:
		-db.collectionName.insertMany([
		     {
	
				"fieldA": "valueA",
				"fieldB": "valueB"

		     },
		     {
	
				"fieldA": "valueA",
				"fieldB": "valueB"

		     },

		])

*/

db.users.insertMany([
	{
		"firstName": "Stephen",
		"lastName": "Hawking",
		"age": 76,
		"email": "stephenhawking@gmail.com",
		"department": "none"
	},
	{
		"firstName": "Neil",
		"lastName": "Armstrong",
		"age": 82,
		"email": "sneilarmstrong@gmail.com",
		"department": "none"
	}

]);

// Mini Activity
	/*
		1. Make a new collection with the name courses
		2. Insert the following fields and values

			name: Javascript 101
			price: 5000
			description: Introduction to Javascript
			isActive: true

			name: HTML 101
			price: 2000
			description: Introduction to HTML
			isActive: true

			name: CSS 101
			price: 2500
			description: Introduction to CSS
			isActive: false

	*/

	db.courses.insertMany([
	{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive": true
	},
	{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
	},
	{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": false
	}

]);

// Find Document (Read)

/*
		Syntax:
			-db.collectionName.find() - this will retrieve all our documents
			-db.collectionName.find({"criteria": "value"}) - will retrieve all documents that match our criteria
			--db.collectionName.findOne({"criteria": "value"}) - will return the first document in our collection that match the criteria
			-db.collectionName.findOne({}) - will return the first document in our collection
*/

db.users.find();

db.users.find({"firstName": "Jane"});

db.users.find(
	{
			"lastName": "Armstrong", 
			"age": 82
	});

// Updating documents(Update)

/*
	Syntax:
		db.collectionName.updateOne(
		{
				"criteria": "field",
		},
		{
				$set: {
					"fieldToBeUpdate": "updatedValue"
				}
			}
		}

		)

*/

db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"department": "none"
});

db.users.updateOne(
	{"firstName": "Test"},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operations",
			"status": "active"
	 	}	
	}
);


// Updating Multiple Documents
db.users.updateMany(
		{
			"name": "Bill"
		},
		{
			$set: {
				"department": "HR"

			}
		}
	);

// Removing a field
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status" : "active"
		}
	}

	)

// Mini Activity
/*
	1. Update the HTML 101 Course
	2. Make the isActive to false
	3. Add enrollees field to all the documents in our courses collection.
		Enrollees: 10
*/

db.courses.updateOne(
		{"name": "HTML 101"},
		{
			"isActive": true
		},
		{
			$set: {
				"isActive": false,

			}
		}
	);

db.courses.updateMany(
	{},
	{
			$set: {
				
				"Enrollees": 10

			}
		}



	)

// Deleting documents (DELETE)

// Document to delete
db.users.insertOne({
	"firstName": "test"
});

// Deleting a single document

/*
	Syntax:
		-db.collectionName.deleteOne ({"criterie": "value"})

*/

db.users.deleteOne({
	"firstName": "test"
});

// Deleting multiple documents

/*
	Syntax:
		-db.collectionName.deleteMany({"criteria": "value"});
*/

db.users.deleteMany({
	"department": "HR"
});

db.courses.deleteMany({});